import uvicorn
import pytest
import requests
if __name__== "__main__":
    uvicorn.run("server.app:app", host="127.0.0.1", port=8000)



def test_read_main():
    response = requests.get("http://127.0.0.1:8000/")
    assert response.status_code == 200
    assert response.json() == {"message": "Welcome to the restaurant menu service"}

def test_read_app():
    response = requests.get("http://127.0.0.1:8000/menu")
    assert response.status_code == 200