from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder
import pytest
import requests
import asyncio
from fastapi.testclient import  TestClient
from app.server.routes.restaurant import router
from app.server.app import app
from app.server.routes.restaurant import (
    add_dish_data,
    get_menu,
    get_menu_data,
    delete_menu_data,
    update_menu_data,
)


client = TestClient(router)


@pytest.mark.asyncio
def test_get_menu():
    response =  requests.get("http://127.0.0.1:8000/menu")
    assert response.status_code == 200

@pytest.mark.asyncio
def test_add_dish_data():
    response = client.post(
        "/",
        json={
            "Dish": " Veg Soup",
            "Category": "Starter",
            "Price": 200,
              }
    )
    json_response = response.json()
    assert response.status_code == 200
    assert json_response['message'] == "dish added successfully."

def test_update_menu_data():
    response = client.put("/601a57c296f1e5c23f8982c8",
                          json = {
                              "Dish": " Veg Soup",
                              "Category": "Starter",
                              "Price": 200,
                               }
                          )
    json_response = response.json()
    assert response.status_code == 200
    assert json_response['message'] == "dish name updated successfully"

def test_delete_menu():
    response = client.delete("/601a57c296f1e5c23f8982c8",
                             )
    json_response = response.json()
    response.status_code == 200
    assert json_response['message'] == "dish deleted successfully"

def test_delete_menu_non_exist():
    response = client.delete("/601a57c296f1e5c23f8982c8",
                             )
    json_response = response.json()
    response.status_code == 404
    assert json_response['message'] == "dish with id 601a57c296f1e5c23f8982c8 doesn't exist"






