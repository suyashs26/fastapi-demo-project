import motor.motor_asyncio

from bson.objectid import ObjectId



MONGO_DETAILS = "mongodb://localhost:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.restaurants

menu_collection = database.get_collection("menu_collection")


def restaurant_helper(restaurant) -> dict:
    return {
        "id": str(restaurant["_id"]),
        "Dish": restaurant["Dish"],
        "Category": restaurant["Category"],
        "Price": restaurant["Price"]
           }
        
# Retrieve all dishes  present in the database
async def retrieve_menu():
    restaurants = []
    async for restaurant in menu_collection.find():
        restaurants.append(restaurant_helper(restaurant))
    return restaurants


# Add a new dish into to the database
async def add_dish(menu_data: dict) -> dict:
    restaurant = await menu_collection.insert_one(menu_data)
    new_dish = await menu_collection.find_one({"_id": restaurant.inserted_id})
    return restaurant_helper(new_dish)


# Retrieve a dish with a matching ID
async def retrieve_dish(id: str) -> dict:
    restaurant = await menu_collection.find_one({"_id": ObjectId(id)})
    if restaurant:
        return restaurant_helper(restaurant)


# Update a dish with a matching ID
async def update_menu(id: str, data: dict):
    # Return false if an empty request body is sent.
    if len(data) < 1:
        return False
    restaurant = await menu_collection.find_one({"_id": ObjectId(id)})
    if restaurant:
        updated_menu = await menu_collection.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_menu:
            return True
        return False


# Delete a dish from the database
async def delete_dish(id: str):
    restaurant = await menu_collection.find_one({"_id": ObjectId(id)})
    if restaurant:
        await menu_collection.delete_one({"_id": ObjectId(id)})
        return True