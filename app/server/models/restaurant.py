# -*- coding: utf-8 -*-

from typing import Optional

from pydantic import BaseModel, Field


class MenuSchema(BaseModel):
    Dish: str = Field(...)
    Category: str = Field(...)
    Price: int = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "Dish": " Veg Soup",
                "Category": "Starter",
                "Price": 200
            }
        }


class UpdateMenuModel(BaseModel):
    Dish: Optional[str]
    Category: Optional[str]
    Price: Optional[int]

    class Config:
        schema_extra = {
            "example": {
                "Dish": " Veg Soup",
                "Category": "Starter",
                "Price": 200
                
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}