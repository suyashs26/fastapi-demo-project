# -*- coding: utf-8 -*-

from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder
import pytest
from app.server.database import (
    add_dish,
    delete_dish,
    retrieve_menu,
    retrieve_dish,
    update_menu,
)
from app.server.models.restaurant import (
    ErrorResponseModel,
    ResponseModel,
    MenuSchema,
    UpdateMenuModel,
)

router = APIRouter()

@router.post("/", response_description="Menu data added into the database")

async def add_dish_data(restaurant: MenuSchema = Body(...)):
    restaurant = jsonable_encoder(restaurant)
    new_dish = await add_dish(restaurant)
    return ResponseModel(new_dish, "dish added successfully.")

@router.get("/", response_description="menu retrieved")
async def get_menu():
    menu = await retrieve_menu()
    if menu:
        return ResponseModel(menu, "menu data retrieved successfully")
    return ResponseModel(menu, "Empty list returned")


@router.get("/{id}", response_description="dish data retrieved")
async def get_menu_data(id):
    dish = await retrieve_dish(id)
    if dish:
        return ResponseModel(dish, "dish data retrieved successfully")
    return ErrorResponseModel("An error occurred.", 404, "dish doesn't exist.")



@router.put("/{id}")
async def update_menu_data(id: str, req: UpdateMenuModel = Body(...)):
    req = {k: v for k, v in req.dict().items() if v is not None}
    updated_menu = await update_menu(id, req)
    if updated_menu:
        return ResponseModel(
            "dish with ID: {} name update is successful".format(id),
            "dish name updated successfully",
        )
    return ErrorResponseModel(
        "An error occurred",
        404,
        "There was an error updating the data.",
    )


@router.delete("/{id}", response_description="dish data deleted from the database")
async def delete_menu_data(id: str):
    deleted_dish = await delete_dish(id)
    if deleted_dish:
        return ResponseModel(
            "dish with ID: {} removed".format(id), "dish deleted successfully"
        )
    return ErrorResponseModel(
        "An error occurred", 404, "dish with id {0} doesn't exist".format(id)
    )