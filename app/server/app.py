# -*- coding: utf-8 -*-

from fastapi import FastAPI
import uvicorn
from app.server.routes.restaurant import router as MenuRouter

app = FastAPI()

app.include_router(MenuRouter, tags=["Menu"], prefix="/menu")

@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Welcome to the restaurant menu service"}

